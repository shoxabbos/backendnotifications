<?php

Route::group(
    [
        'prefix' => '/backend/studiobosco/backendnotifications/api',
        'middleware' => ['web'],
    ], function () {
        Route::get('count', 'StudioBosco\BackendNotifications\Http\Api@count');
        Route::get('latest', 'StudioBosco\BackendNotifications\Http\Api@latest');
});
