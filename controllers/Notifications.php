<?php namespace StudioBosco\BackendNotifications\Controllers;

use Input;
use BackendMenu;
use Backend\Classes\Controller;
use StudioBosco\BackendNotifications\Helpers\BackendNotifications;

/**
 * Notifications Back-end Controller
 */
class Notifications extends Controller
{
    public $implement = [
        'Backend.Behaviors.ListController'
    ];

    public $listConfig = 'config_list.yaml';

    public $requiredPermissions = [
        'studiobosco.backendnotifications::manage_notifications',
    ];

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('StudioBosco.BackendNotifications', 'backendnotifications', 'notifications');
        $this->addCss('/plugins/studiobosco/backendnotifications/assets/css/backendnotifications.css');
    }

    public function listExtendQuery($query)
    {
        $query->listBackend();
        return $query;
    }

    public function onRead()
    {
        $recordId = Input::get('record_id');

        if (!$recordId) {
            return;
        }

        BackendNotifications::read($recordId);

        return $this->listRefresh();
    }

    public function onReadAll()
    {
        $record = BackendNotifications::readAll();
        return $this->listRefresh();
    }
}
