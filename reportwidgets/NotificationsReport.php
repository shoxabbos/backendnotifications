<?php namespace StudioBosco\BackendNotifications\ReportWidgets;

use Input;
use Backend\Classes\ReportWidgetBase;
use StudioBosco\BackendNotifications\Models\Notification;
use StudioBosco\BackendNotifications\Helpers\BackendNotifications;

class NotificationsReport extends ReportWidgetBase
{
	public function loadData() {
		$this->vars['notifications'] = $notifications = Notification::listBackend()->limit(10)->get();
	}


    public function render()
    {
    	$this->loadData();

        return $this->makePartial('notifications');
    }


    public function defineProperties()
    {
        return [
            'title' => [
                'title'             => 'Widget Title',
                'default'           => 'Notifications',
                'type'              => 'string',
                'validationPattern' => '^.+$',
            ]
        ];
    }

	public function onRead()
    {
        $recordId = Input::get('record_id');

        if (!$recordId) {
            return;
        }

        BackendNotifications::read($recordId);

        $this->loadData();


        return [
        	'partial' => $this->makePartial('notifications', ['notifications' => $this->vars['notifications']])
        ];
    }




}